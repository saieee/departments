--simple loop
declare
a number(10):=1;
begin
loop
dbms_output.put_line(a);
exit when a>=10;
a:=a+1;
end loop;
end;

--while loop

declare
a number(10):=1;
begin
while a<=10 loop
dbms_output.put_line(a);
a:=a+1;
end loop;
end;

--for loop

declare
i number(10);
begin
for i in 1..10 loop
dbms_output.put_line(i);
end loop;
end;